package fr.vallefort.springintegration.filetokafka.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.vallefort.springintegration.filetokafka.beans.Client;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

@Slf4j
public class ClientDeserializer implements Deserializer<Client> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Deserializer.super.configure(configs, isKey);
    }

    @Override
    public Client deserialize(String topic, byte[] bytes) {
        //log.debug("Début de process de deserialisation ");
        String json = new String(bytes);
        Client dataMessage = null;
        try {
            dataMessage = mapper.readValue(json, Client.class);
        } catch (Exception e) {
            //log.error("Erreur process de deserialisation ", e);
        }
        return dataMessage;
    }

    @Override
    public Client deserialize(String topic, Headers headers, byte[] data) {
        return Deserializer.super.deserialize(topic, headers, data);
    }

    @Override
    public void close() {
        Deserializer.super.close();
    }
}
