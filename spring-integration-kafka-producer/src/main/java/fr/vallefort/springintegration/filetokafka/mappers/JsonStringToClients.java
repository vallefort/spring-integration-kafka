package fr.vallefort.springintegration.filetokafka.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.vallefort.springintegration.filetokafka.beans.Client;
import org.springframework.integration.annotation.Splitter;

import java.util.List;

public class JsonStringToClients {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public JsonStringToClients() {

    }

    @Splitter
    public List<Client> jsonStringToClients(String jsonString) throws JsonProcessingException {

        return objectMapper.readValue(jsonString, new TypeReference<>() {
        });

    }
}
