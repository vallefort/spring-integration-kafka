package fr.vallefort.springintegration.filetokafka.integration;

import fr.vallefort.springintegration.filetokafka.beans.Client;
import fr.vallefort.springintegration.filetokafka.mappers.JsonStringToClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.file.filters.AcceptOnceFileListFilter;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.integration.kafka.dsl.KafkaProducerMessageHandlerSpec;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.DefaultKafkaHeaderMapper;

import java.io.File;

@Configuration
public class IntegrationConfiguration {

    private final ProducerFactory<Integer, Client> producerFactory;

    public IntegrationConfiguration(ProducerFactory<Integer, Client> producerFactory) {
        this.producerFactory = producerFactory;
    }

    @Bean
    public IntegrationFlow fileJsonInputIntegrationFlow() {
        return IntegrationFlows.from(Files.inboundAdapter(new File("D:/test/client/input/json"))
                                .filter(new SimplePatternFileListFilter("*-ready.json"))
                                .filter(new AcceptOnceFileListFilter<>()),
                        e -> e.poller(Pollers.fixedDelay(1000)))
                .transform(Files.toStringTransformer())
                .split(new JsonStringToClients()).handle(kafkaMessageHandler("topic-clients"))
                .get();
    }

    private KafkaProducerMessageHandlerSpec<Integer, Client, ?> kafkaMessageHandler(String topic) {
        return Kafka
                .outboundChannelAdapter(producerFactory)
                .messageKey(m -> m
                        .getHeaders()
                        .get(IntegrationMessageHeaderAccessor.SEQUENCE_NUMBER))
                .headerMapper(new DefaultKafkaHeaderMapper())
                .partitionId(m -> 0)
                .topicExpression("headers[kafka_topic] ?: '" + topic + "'")
                .configureKafkaTemplate(t -> t.id("kafkaTemplate:" + topic));
    }
}
