package fr.vallefort.springintegration.filetokafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileToKafkaTopicApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileToKafkaTopicApplication.class, args);
    }

}
