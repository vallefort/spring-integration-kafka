package fr.vallefort.springintegration.filetokafka.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.vallefort.springintegration.filetokafka.beans.Client;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

@Slf4j
public class ClientSerializer implements Serializer<Client> {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Serializer.super.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String s, Client dataMessage) {
        //log.debug("Processus de serialisation de l'object GetUnit {}", dataMessage);
        String json = "";
        try {
            json = objectMapper.writeValueAsString(dataMessage);
        } catch (Exception e) {
            //log.error("Erreur dans le process de serialization ", e);
        }
        return json.getBytes();
    }

    @Override
    public byte[] serialize(String topic, Headers headers, Client data) {
        return Serializer.super.serialize(topic, headers, data);
    }

    @Override
    public void close() {
        Serializer.super.close();
    }
}
