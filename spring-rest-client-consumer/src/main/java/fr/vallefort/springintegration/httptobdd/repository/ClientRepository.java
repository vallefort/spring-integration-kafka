package fr.vallefort.springintegration.httptobdd.repository;


import fr.vallefort.springintegration.httptobdd.beans.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Integer> {
}
