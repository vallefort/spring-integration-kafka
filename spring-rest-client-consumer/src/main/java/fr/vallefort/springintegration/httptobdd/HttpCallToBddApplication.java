package fr.vallefort.springintegration.httptobdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpCallToBddApplication {

    public static void main(String[] args) {
        SpringApplication.run(HttpCallToBddApplication.class, args);
    }

}
