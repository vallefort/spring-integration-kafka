package fr.vallefort.springintegration.httptobdd.controller;

import fr.vallefort.springintegration.httptobdd.beans.Client;
import fr.vallefort.springintegration.httptobdd.repository.ClientRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientRestController {

    private final ClientRepository repo;

    public ClientRestController(ClientRepository clientRepository) {
        this.repo = clientRepository;
    }

    @PostMapping("/client")
    public ResponseEntity<String> createClient(@RequestBody Client client) {
        repo.save(client);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
