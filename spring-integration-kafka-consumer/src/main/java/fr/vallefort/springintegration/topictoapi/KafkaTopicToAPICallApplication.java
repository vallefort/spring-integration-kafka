package fr.vallefort.springintegration.topictoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaTopicToAPICallApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaTopicToAPICallApplication.class, args);
    }

}
