package fr.vallefort.springintegration.topictoapi.beans;

import java.sql.Date;


public class Client {

    private Integer id;

    private Integer reference;

    private Date dateCreation;

    private String nom;

    private String prenom;

    public Client() {

    }

    public Client(Integer reference, Date dateCreation, String nom, String prenom) {
        this.reference = reference;
        this.dateCreation = dateCreation;
        this.nom = nom;
        this.prenom = prenom;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
