package fr.vallefort.springintegration.topictoapi.integration;

import fr.vallefort.springintegration.topictoapi.beans.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.http.dsl.Http;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConsumerProperties;

@Configuration
public class IntegrationConfiguration {

    private final ConsumerFactory<Integer, Client> consumerFactory;

    public IntegrationConfiguration(ConsumerFactory<Integer, Client> consumerFactory) {
        this.consumerFactory = consumerFactory;
    }


    @Bean
    public IntegrationFlow fileJsonInputIntegrationFlow() {
        return IntegrationFlows.from(Kafka.inboundChannelAdapter(consumerFactory, new ConsumerProperties("topic-clients"))
                        , e -> e.poller(Pollers.fixedDelay(5000)))
                .handle(Http.outboundGateway("http://localhost:8080/client")
                        .httpMethod(HttpMethod.POST)
                        .expectedResponseType(String.class))
                .get();
    }
}
